﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IBE.Plugins
{
    /// <summary>
    /// Объект, содержащий логическую структуру проекта
    /// </summary>
    public interface IProject
    {
    }

    /// <summary>
    /// Объект, управляющий физическим состоянием проекта
    /// </summary>
    public interface IProjectLoader
    {
        bool CanLoad(string projectPath);
        IProject Load(string projectPath);

        bool CanSave(IProject project);
        void Save(IProject project, string projectPath);

        bool CanInit(string projectPath);
        IProject Init(string projectPath);
    }

    /// <summary>
    /// Интерфейс, необходимый для подключения загрузчиков конкретных проектов
    /// </summary>
    /// <typeparam name="TProject"></typeparam>
    public interface IProjectLoader<TProject> : IProjectLoader
        where TProject : IProject
    {
        
    }

    /// <summary>
    /// Объект взаимодействия между формой и проектом
    /// </summary>
    /// <typeparam name="TForm"></typeparam>
    public interface IProjectView<TForm>
        where TForm : IForm
    {
        bool CanView(IProject manager);
        void View(TForm form, IProject loader);
    }

    /// <summary>
    /// Представляет форму
    /// </summary>
    public interface IForm
    {

    }

    /// <summary>
    /// Плагин, расширяющий функционал формы до указанной
    /// </summary>
    /// <typeparam name="TFrom"></typeparam>
    /// <typeparam name="TTo"></typeparam>
    public interface IUpgrade<TFrom, TTo>
    {
        TTo Upgrade(TFrom tfrom);
    }

}
