﻿using System;

namespace IBE.Desktop.Default
{
    /// <summary>
    /// Интерфейс объектов, обладающих именем
    /// </summary>
    public interface IRename
    {
        string Name
        {
            get;
        }

        void Rename(string str);
        event EventHandler Renamed;
    }
}