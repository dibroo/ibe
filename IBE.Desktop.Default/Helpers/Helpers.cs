﻿using System.Windows.Forms;

namespace IBE.Desktop.Default
{
    public static class Helpers
    {
        /// <summary>
        /// Вызывает окно для ввода строки
        /// </summary>
        /// <param name = "old">Начальное значение поля ввода</param>
        /// <returns>Строка, введённая в форме</returns>
        public static string CallEditForm(string old)
        {
            var val = old;
            var f = new StringEditForm();
            (f.Controls["EditBox"] as TextBox).Text = old;
            (f.Controls["AcceptButton"] as Button).Click += (s, e) =>
            {
                val = (f.Controls["EditBox"] as TextBox).Text;
                f.Close();
            }

            ;
            f.ShowDialog();
            return val;
        }

        /// <summary>
        /// Создаёт кнопку контекстного меню для переименования заданного объекта
        /// </summary>
        /// <param name = "renambl">Объект, который нужно переименовывать</param>
        /// <returns>Кнопка меню</returns>
        public static ToolStripMenuItem MakeRenameButton(IRename renambl)
        {
            var renameButton = new ToolStripMenuItem("Переименовать");
            renameButton.Click += (s, e) =>
            {
                var name = CallEditForm(renambl.Name);
                renambl.Rename(name);
            }

            ;
            return renameButton;
        }

        public static void RemoveTyped<T>(this TreeNodeCollection nodes, T node)
        {
            int i;
            for (i = 0; i < nodes.Count; i++)
            {
                var bookNode = nodes[i] as TypedTreeNode<T>;
                if (bookNode != null && bookNode.IsContains(node))
                    break;
            }

            if (i == nodes.Count)
                return;
            nodes.RemoveAt(i);
        }
    }
}