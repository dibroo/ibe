﻿using System;
using System.IO;

namespace IBE.Desktop.Default
{
    public class Scene : IRename
    {
        public Scene(string name, string path)
        {
            Name = name;
            file = new FileInfo(path);
            if (File.Exists(path))
                Text = File.ReadAllText(path);
            else
            {
                if (!Directory.Exists(Path.GetDirectoryName(path)))
                    Directory.CreateDirectory(Path.GetDirectoryName(path));
                File.WriteAllText(path, "");
            }
        }

        public Scene(string name)
        {
            Name = name;
            Text = "";
        }

        public void Save(string path)
        {
            File.WriteAllText(path, Text);
        }

        public void Save()
        {
            File.WriteAllText(file.FullName, Text);
        }

        public FileInfo file;
        public event EventHandler Renamed;
        public string Name
        {
            get;
            private set;
        }

        public string Text
        {
            get;
            set;
        }

        public void Rename(string str)
        {
            Name = str;
            Renamed?.Invoke(this, EventArgs.Empty);
        }
    }
}