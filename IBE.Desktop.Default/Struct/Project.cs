﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using IBE.Plugins;

namespace IBE.Desktop.Default
{
    /// <summary>
    /// Класс стандартного проекта
    /// </summary>
    public class Project : IRename, IProject
    {
        /// <summary>
        /// Событие, вызываемое при изменении имени
        /// </summary>
        public event EventHandler Renamed;
        /// <summary>
        /// Событие, вызываемое при удалении книги из проекта
        /// </summary>
        public event EventHandler<BookEventArgs> BookRemoved;
        /// <summary>
        /// Событие, вызываемое при добавлении книги в проект
        /// </summary>
        public event EventHandler<BookEventArgs> BookAdded;
        /// <summary>
        /// Книги, входящие в проект
        /// </summary>
        public List<Book> books = new List<Book>();
        public Project(string name, string path)
        {
            Name = name;
            Path = path;
        }

        /// <summary>
        /// Имя проекта
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        public string Path
        {
            get;
            private set;
        }

        /// <summary>
        /// Добавляет книгу
        /// </summary>
        /// <param name = "name">Имя книги</param>
        public void AddBook(string name)
        {
            var book = new Book(name);
            books.Add(book);
            BookAdded?.Invoke(this, new BookEventArgs(book));
        }

        /// <summary>
        /// Удаляет книгу
        /// </summary>
        /// <param name = "name">Имя книги</param>
        public void RemoveBook(string name)
        {
            var book = books.FirstOrDefault(x => x.Name == name);
            if (book != null)
            {
                books.Remove(book);
                BookRemoved?.Invoke(this, new BookEventArgs(book));
            }
        }

        /// <summary>
        /// Переименовывает проект
        /// </summary>
        /// <param name = "name">Новое имя</param>
        public void Rename(string name)
        {
            Name = name;
            Renamed?.Invoke(this, EventArgs.Empty);
        }
    }
}