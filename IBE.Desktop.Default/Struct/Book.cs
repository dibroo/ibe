﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace IBE.Desktop.Default
{
    public class Book : IRename
    {
        public event EventHandler Renamed;
        public event EventHandler<ChapterEventArgs> ChapterAdded;
        public event EventHandler<ChapterEventArgs> ChapterRemoved;
        public List<Chapter> chapters = new List<Chapter>();
        public Book(string name)
        {
            Name = name;
        }

        public string Name
        {
            get;
            private set;
        }

        public void Rename(string name)
        {
            Name = name;
            Renamed?.Invoke(this, EventArgs.Empty);
        }

        public void AddChapter(string name)
        {
            var chapter = new Chapter(name);
            chapters.Add(chapter);
            ChapterAdded?.Invoke(this, new ChapterEventArgs(chapter));
        }

        public void RemoveChapter(string name)
        {
            var chapter = chapters.FirstOrDefault(x => x.Name == name);
            if (chapter != null)
            {
                chapters.Remove(chapter);
                ChapterRemoved?.Invoke(this, new ChapterEventArgs(chapter));
            }
        }
    }
}