﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace IBE.Desktop.Default
{
    public class Chapter : IRename
    {
        public string Name
        {
            get;
            private set;
        }

        public List<Scene> scenes = new List<Scene>();
        public event EventHandler Renamed;
        public event EventHandler<SceneEventArgs> SceneAdded;
        public event EventHandler<SceneEventArgs> SceneRemoved;
        public Chapter(string name)
        {
            Name = name;
        }

        public void Rename(string str)
        {
            Name = str;
            Renamed?.Invoke(this, EventArgs.Empty);
        }

        public void AddScene(string name)
        {
            var scene = new Scene(name);
            scenes.Add(scene);
            SceneAdded?.Invoke(this, new SceneEventArgs(scene));
        }

        public void AddScene(string name, string path)
        {
            var scene = new Scene(name, path);
            scenes.Add(scene);
            SceneAdded?.Invoke(this, new SceneEventArgs(scene));
        }

        public void RemoveScene(string name)
        {
            var scene = scenes.FirstOrDefault(x => x.Name == name);
            if (scene != null)
            {
                scenes.Remove(scene);
                SceneRemoved?.Invoke(this, new SceneEventArgs(scene));
            }
        }
    }
}