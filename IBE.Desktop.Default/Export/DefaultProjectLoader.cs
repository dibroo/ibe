﻿using System;
using System.Linq;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.IO;
using IBE.Desktop.Core;
using IBE.Plugins;

namespace IBE.Desktop.Default
{
    /// <summary>
    /// Загрузчик стандартного проекта
    /// </summary>
    [Export(typeof (IProjectLoader))]
    public class DefaultProjectLoader : IProjectLoader
    {
        public IProject Init(string path)
        {
            var wizard = new ProjectWizard();
            var dr = wizard.ShowDialog();

            if (dr != System.Windows.Forms.DialogResult.OK)
                return new CancelProject();

            var name = wizard.ProjectName;
            File.WriteAllText(path, $"$project\n{name}");
            wizard.Dispose();
            return new Project(name, path);
        }
        public IProject Load(string path)
        {
            var lines = File.ReadAllLines(path);
            Project project = null;
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i] == "$project")
                    project = new Project(lines[i + 1], path);
                if (lines[i] == "$book")
                    project.books.Add(new Book(lines[i + 1]));
                if (lines[i] == "$chapter")
                    project.books.Last().chapters.Add(new Chapter(lines[i + 1]));
                if (lines[i] == "$scene")
                    project.books.Last().chapters.Last().scenes.Add(new Scene(lines[i + 1], lines[i + 2]));
            }

            return project;
        }

        public void Save(IProject root, string path)
        {
            var p = root as Project;
            var projectFile = File.CreateText(Path.Combine(path, p.Name + ".dproj"));
            projectFile.WriteLine("$project");
            projectFile.WriteLine(p.Name);

            foreach (var book in p.books)
            {
                projectFile.WriteLine("$book");
                projectFile.WriteLine(book.Name);
                foreach (var chapter in book.chapters)
                {
                    projectFile.WriteLine("$chapter");
                    projectFile.WriteLine(chapter.Name);
                    foreach (var scene in chapter.scenes)
                    {
                        var newPath = Path.Combine(book.Name, chapter.Name, scene.Name + ".txt");
                        scene.file.MoveTo(newPath);

                        scene.Save();

                        projectFile.WriteLine("$scene");
                        projectFile.WriteLine(scene.Name);
                    }
                }
            }

            projectFile.Close();
        }

        public bool CanLoad(string projectPath)
        {
            // TODO
            return true;
        }

        public bool CanSave(IProject project)
        {
            // TODO
            return true;
        }

        public bool CanInit(string projectPath)
        {
            // TODO
            return true;
        }
    }
}