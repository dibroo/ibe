﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Forms;
using IBE.Desktop.Core;
using IBE.Plugins;
using System.Diagnostics;

namespace IBE.Desktop.Default
{
    /// <summary>
    /// Представление стандартного проекта
    /// </summary>
    [Export(typeof (IProjectView<IDefaultDesktopForm>))]
    public class DefaultProjectView : IProjectView<IDefaultDesktopForm>
    {
        public bool CanView(IProject manager)
        {
            // TODO
            return true;
        }

        public void View(IDefaultDesktopForm form, IProject project)
        {
            var pr = project as Project;

            var tp = new TabPage(pr.Name);
            var tv = new ProjectTreeView(pr);

            tv.Dock = DockStyle.Fill;
            tp.Controls.Add(tv);

            form.Project.TabPages.Add(tp);
        }
    }
}