﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IBE.Desktop.Default
{
    public partial class StringEditForm : Form
    {
        public StringEditForm()
        {
            InitializeComponent();
        }

        public string Value { get { return EditBox.Text; } set { EditBox.Text = value; } }

        private void AcceptButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
