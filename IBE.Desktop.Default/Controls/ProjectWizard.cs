﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IBE.Desktop.Default
{
    public partial class ProjectWizard : Form
    {
        public ProjectWizard()
        {
            InitializeComponent();
        }

        public string ProjectName => projectName.Text;

        private void finishButton_Click(object sender, EventArgs e)
        {
            Close();
            DialogResult = DialogResult.OK;
        }
    }
}
