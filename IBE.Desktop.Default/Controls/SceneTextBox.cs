﻿using System.Windows.Forms;

namespace IBE.Desktop.Default
{
    public class SceneTextBox : RichTextBox
    {
        private Scene _scene;
        public SceneTextBox(Scene scene)
        {
            _scene = scene;
        }

        public override string Text
        {
            get
            {
                return _scene.Text;
            }

            set
            {
                _scene.Text = value;
            }
        }
    }
}