﻿using System.Windows.Forms;

namespace IBE.Desktop.Default
{
    public class SceneTabPage : TabPage
    {
        private Scene _scene;
        public SceneTabPage(Scene scene)
        {
            _scene = scene;
            var tb = new SceneTextBox(scene);
            tb.Dock = DockStyle.Fill;
            Controls.Add(tb);
        }
    }
}