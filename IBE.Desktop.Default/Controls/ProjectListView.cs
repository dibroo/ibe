﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IBE.Desktop.Default
{
    public class ProjectTreeView : TreeView
    {
        private Project _proj;

        public ProjectTreeView(Project proj)
        {
            _proj = proj;
            var lv = new ProjectTreeNode(proj);
            Nodes.Add(lv);

            base.MouseUp += ProjectTreeView_MouseUp;
        }

        private void ProjectTreeView_MouseUp(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{
            //    SelectedNode = GetNodeAt(e.X, e.Y);

            //    if (SelectedNode != null)
            //    {
            //        var s = SelectedNode.ContextMenuStrip;
            //        s.ResumeLayout(false);    
            //        s.Show(this, e.Location);
            //        Update();
            //    }
            //}
        }
    }
}
