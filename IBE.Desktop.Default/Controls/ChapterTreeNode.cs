﻿using System;
using System.Linq;
using System.IO;
using System.Windows.Forms;

namespace IBE.Desktop.Default
{
    /// <summary>
    /// Узел TreeNode, содержащий главу
    /// </summary>
    public class ChapterTreeNode : TypedTreeNode<Chapter>
    {
        public ChapterTreeNode(Chapter chapter): base (chapter)
        {
            Text = chapter.Name;
            foreach (var scene in chapter.scenes)
                Nodes.Add(new SceneTreeNode(scene));

            chapter.Renamed += Chapter_Renamed;
            chapter.SceneAdded += Chapter_SceneAdded;
            chapter.SceneRemoved += Chapter_SceneRemoved;

            ContextMenuStrip = new ChapterContextMenu(this);
        }

        private void Chapter_SceneRemoved(object sender, SceneEventArgs e)
        {
            Nodes.RemoveTyped(e.Obj);
        }

        private void Chapter_SceneAdded(object sender, SceneEventArgs e)
        {
            Nodes.Add(new SceneTreeNode(e.Obj));
        }

        private void Chapter_Renamed(object sender, EventArgs e)
        {
            var c = sender as Chapter;
            Text = c.Name;
        }

        private string ConstructPath()
        {
            var root = Parent;
            var path = _encObj.Name;
            while (!(root is ProjectTreeNode))
            {
                path = Path.Combine(root.Name, path);
                root = root.Parent;
            }
            if (root == null)
                return "";

            var pp = (root as ProjectTreeNode).Project.Path;
            var rpath = Path.GetDirectoryName(pp);
            return Path.Combine(rpath, path);
            
        }

        class ChapterContextMenu : ContextMenuStrip
        {
            private ChapterTreeNode node;

            public ChapterContextMenu(ChapterTreeNode node)
            {
                this.node = node;
                var rename = Helpers.MakeRenameButton(node._encObj);
                var addScene = new ToolStripMenuItem("Добавить сцену");

                addScene.Click += AddScene_Click;

                Items.Add(rename);
                Items.Add(addScene);
            }

            private void AddScene_Click(object sender, EventArgs e)
            {
                var path = node.ConstructPath();

                var sceneName = Helpers.CallEditForm("");
                var sceneFile = Path.Combine(path, sceneName + ".txt");

                node._encObj.AddScene(sceneName, sceneFile);
            }
        }
    }
}