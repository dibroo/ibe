﻿using System;

namespace IBE.Desktop.Default
{
    /// <summary>
    /// Узел TreeView, представляющий корень проекта
    /// </summary>
    public class ProjectTreeNode : TypedTreeNode<Project>
    {
        public Project Project => _encObj;
        public ProjectTreeNode(Project proj): base (proj)
        {
            // Устанавливаем имя и подписываемся на событие, чтобы менять подпись узла при изменениии имени.
            Text = proj.Name;
            proj.Renamed += Proj_Renamed;
            // Создаём контекстное меню для узла.
            base.ContextMenuStrip = new ProjectContextMenu(proj);
            
            // Создаём узлы
            foreach (var book in proj.books)
                Nodes.Add(new BookTreeNode(book));
            // Если добавляем новую книгу, то добавляем и новый узел
            proj.BookAdded += Proj_BookAdded;
            // Если удаляем книгу, то удаляем и соответсвующий узел
            // TODO: Возможно стоит выводить окно о подтверждении удаления или просить сохранить копию в другое место (ДОБАВИТЬ СИСТЕМУ КОНТРОЛЯ ВЕРСИЙ) 
            proj.BookRemoved += Proj_BookRemoved;
        }

        private void Proj_BookRemoved(object sender, BookEventArgs e)
        {
            Nodes.RemoveTyped(e.Obj);
        }

        private void Proj_BookAdded(object sender, BookEventArgs e)
        {
            Nodes.Add(new BookTreeNode(e.Obj));
        }

        private void Proj_Renamed(object sender, EventArgs e)
        {
            // Просто меняем подпись при имзенении имени проекта
            var p = sender as Project;
            Text = p.Name;
        }
    }
}