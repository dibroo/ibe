﻿namespace IBE.Desktop.Default
{
    /// <summary>
    /// Узел TreeNode, содержащий книгу
    /// </summary>
    public class SceneTreeNode : TypedTreeNode<Scene>
    {
        public SceneTreeNode(Scene scene): base (scene)
        {
            Text = scene.Name;
        }
    }
}