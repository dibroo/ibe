﻿using System.Windows.Forms;

namespace IBE.Desktop.Default
{
    /// <summary>
    /// Контекстное меню, содержащее функции для работы с проектом
    /// </summary>
    public class ProjectContextMenu : ContextMenuStrip
    {
        Project _project;

        public ProjectContextMenu(Project project)
        {
            _project = project;

            var renameButton = Helpers.MakeRenameButton(project);
            var addBookButton = new ToolStripMenuItem("Добавить книгу");

            addBookButton.Click += AddBookButton_Click;

            Items.Add(renameButton);
            Items.Add(addBookButton);
        }

        private void AddBookButton_Click(object sender, System.EventArgs e)
        {
            var editor = new StringEditForm();
            var res = editor.ShowDialog();

            if(res == DialogResult.OK)
                _project.AddBook(editor.Value);

            editor.Dispose();
        }
    }
}