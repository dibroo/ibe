﻿using System.Windows.Forms;

namespace IBE.Desktop.Default
{
    public class TypedTreeNode<T> : TreeNode
    {
        protected T _encObj;
        public TypedTreeNode(T obj)
        {
            _encObj = obj;
        }

        public bool IsContains(T obj) => Equals(_encObj, obj);
    }
}