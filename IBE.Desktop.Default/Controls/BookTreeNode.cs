﻿using System;

namespace IBE.Desktop.Default
{
    /// <summary>
    /// Узел TreeNode, содержащий книгу
    /// </summary>
    public class BookTreeNode : TypedTreeNode<Book>
    {
        public BookTreeNode(Book book): base (book)
        {
            Text = book.Name;
            foreach (var chap in book.chapters)
                Nodes.Add(new ChapterTreeNode(chap));
            book.Renamed += Book_Renamed;
            book.ChapterAdded += Book_ChapterAdded;
            book.ChapterRemoved += Book_ChapterRemoved;
        }

        private void Book_ChapterRemoved(object sender, ChapterEventArgs e)
        {
            Nodes.RemoveTyped(e.Obj);
        }

        private void Book_ChapterAdded(object sender, ChapterEventArgs e)
        {
            Nodes.Add(new ChapterTreeNode(e.Obj));
        }

        private void Book_Renamed(object sender, EventArgs e)
        {
            var b = sender as Book;
            Text = b.Name;
        }
    }
}