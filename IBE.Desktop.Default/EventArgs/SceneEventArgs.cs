﻿namespace IBE.Desktop.Default
{
    public class SceneEventArgs : TypedEventArgs<Scene>
    {
        public SceneEventArgs(Scene s): base (s)
        {
        }
    }
}