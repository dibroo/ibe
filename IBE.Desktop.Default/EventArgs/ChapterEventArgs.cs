﻿namespace IBE.Desktop.Default
{
    public class ChapterEventArgs : TypedEventArgs<Chapter>
    {
        public ChapterEventArgs(Chapter c): base (c)
        {
        }
    }
}