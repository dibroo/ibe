﻿namespace IBE.Desktop.Default
{
    public class BookEventArgs : TypedEventArgs<Book>
    {
        public BookEventArgs(Book b): base (b)
        {
        }
    }
}