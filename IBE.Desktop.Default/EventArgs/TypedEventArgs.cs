﻿using System;

namespace IBE.Desktop.Default
{
    public class TypedEventArgs<T> : EventArgs
    {
        public readonly T Obj;
        public TypedEventArgs(T obj)
        {
            Obj = obj;
        }
    }
}