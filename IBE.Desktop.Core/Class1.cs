﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.ComponentModel;

using IBE.Plugins;
using System.Runtime.Serialization;

namespace IBE.Desktop.Core
{
    public interface IDefaultDesktopForm : IForm
    {
        TabControl Middle { get; }
        TabControl Project { get; }
        TabControl Bottom { get; }

        MenuStrip Menu { get; }
    }

    public class CancelProject : IProject
    {
        public CancelProject() { }
        public CancelProject(string reason)
        {
            Reason = reason;
        }

        public string Reason { get; private set; }
    }

    public class ErrorProject : Exception, IProject
    {
        public ErrorProject(Exception exc)
        {
            Error = exc;
        }

        public override IDictionary Data => Error.Data;
        public override Exception GetBaseException() => Error;
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            Error.GetObjectData(info, context);
        }

        public override string HelpLink
        {
            get
            {
                return Error.HelpLink;
            }

            set
            {
                Error.HelpLink = value;
            }
        }

        public override string Message => "Загрузка или создание проекта завершилось с ошибкой. См. значение свойства Error, чтобы узнать подробности";

        public override string Source
        {
            get
            {
                return Error.Source;
            }

            set
            {
                Error.Source = value;
            }
        }

        public override string StackTrace
        {
            get
            {
                return Error.StackTrace;
            }
        }

        public Exception Error { get; private set; }
    }
    
}
