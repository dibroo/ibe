﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;

using IBE.Desktop.Core;
using IBE.Plugins;

namespace IBE.Desktop
{
    public partial class Form1 : Form, IDefaultDesktopForm
    {
        [ImportMany(typeof(IProjectLoader))]
        private IEnumerable<IProjectLoader> Loaders { get; set; }

        [ImportMany(typeof(IProjectView<IDefaultDesktopForm>))]
        private IEnumerable<IProjectView<IDefaultDesktopForm>> Views { get; set; }

        public TabControl Middle => middleTabControl;

        public TabControl Project => leftTabControl;

        TabControl IDefaultDesktopForm.Bottom => bottomTabControl;

        MenuStrip IDefaultDesktopForm.Menu => mainMenuStrip;

        private CompositionContainer _container;

        public Form1()
        {
            InitializeComponent();
            LoadPlugins();
        }

        private void LoadPlugins()
        {
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog("./"));
            _container = new CompositionContainer(catalog);

            _container.ComposeParts(this);
        }

        private void openProjectDialog_FileOk(object sender, CancelEventArgs e)
        {
            var pd = sender as FileDialog;
            var ext = Path.GetExtension(pd.FileName).Substring(1);

            var loader = Loaders.FirstOrDefault(x => x.CanLoad(pd.FileName));
            var pr = loader.Load(pd.FileName);

            var view = Views.FirstOrDefault(x => x.CanView(pr));
            view.View(this, pr);
        }

        private void tsmiOpenProject_Click(object sender, EventArgs e)
        {
            openProjectDialog.ShowDialog();
        }

        private void initProjectDialog_FileOk(object sender, CancelEventArgs e)
        {
            var pd = sender as FileDialog;

            var loader = Loaders.FirstOrDefault(x => x.CanLoad(pd.FileName));
            var root = loader.Init(Path.GetFullPath(pd.FileName));

            var view = Views.FirstOrDefault(x => x.CanView(root));
            view.View(this, root);
        }

        private void tsmiNewProject_Click(object sender, EventArgs e)
        {
            initProjectDialog.ShowDialog();
        }
    }
}
